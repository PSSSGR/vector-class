/*
 * Filename:	main.cpp 
 * Name:		Sri Padala
 * Description:	This program is about implementing a weak version of the vector class, which will be a dynamic array of string variables.
 */


#include "test_interface.hpp"

int main()
{
	runTests();

	return 0;
}
