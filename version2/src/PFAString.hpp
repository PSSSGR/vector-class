/*
 * Filename:	PFAString.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a weak version of the vector class, which will be a dynamic array of string variables.
 */

#ifndef PFASTRING_HPP
#define PFASTRING_HPP

#include<string>
class PFAString{

public:
    PFAString();//Default Constructor.
    PFAString(int cap);//Constructor that sets capacity
    // Constructor that sets capacity and initializes all elements to a specific string
    PFAString(int capValue,const std::string& word);
    // Copy Constructor
    PFAString(const PFAString& PFAStringObject);
    //Move Constructor
    PFAString(PFAString&&  PFAStringObject);
    // Destructor
    ~PFAString();

    class zero_size {};

    int capacity() const;//returns capacity
    int size() const;//returns size
    std::string& at(int i);
    void set_capacity(int cap) { _capacity = cap; }//sets capacity with given value
    void set_size(int n) { _size = n; }//sets size with given value
    void set_arr(std::string *str) { arr = str; }

    // Places a string at the back of the array
	void push_back( std::string tempStr);
	// Destroys the string at the back of the array
	void pop_back();
    void reserve(int n, bool copy);
    void resize(int = 0);// Grows or shrinks array accordingly, must behave like vector resize() function
	void resize(int size, std::string tempStr);
    void empty_array(); // Sets the size to zero
    std::string& operator [](int pos);    //  works as l-value
    PFAString& operator = (const PFAString& rSide) ;    //  works as l-value
    PFAString& operator = (PFAString&& rSide);
    bool operator ==(const PFAString& object) const;//relational operators
    bool operator !=(const PFAString& object) const;
	bool operator <(const PFAString& object) const;
	bool operator >(const PFAString& object) const;
	bool operator <=(const PFAString& object) const;
	bool operator >=(const PFAString& object) const;

private:
    std::string *arr; //Pointer to array of strings
	int _capacity; // Max Array can hold
	int _size;	// Amount currently holding
	void grow(); // Inline function use to grow the capacity of new objects.
};
#endif
