
# Filename:	Makefile 
# Name:		Sri Padala
# Description:This program is about implementing a weak version of the vector class, which will be a dynamic array of string variables.
 
CXX = g++
CXXFLAGS = -Wall -std=c++11

EXE = hw05

SRCDIR = src
SRCS = PFAString.cpp main.cpp
OBJS = $(SRCS:%.cpp=%.o)
OBJS := $(addprefix $(SRCDIR)/, $(OBJS))

.PHONY : all clean

all : $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(EXE) $^

$(SRCDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean :
	rm -f $(OBJS) core $(EXE)
